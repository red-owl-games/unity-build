#!/bin/bash

mkdir -p ./scripts
curl -sL -o ./scripts/clean.sh https://gitlab.com/red-owl-games/unity-build/raw/master/scripts/clean.sh
curl -sL -o ./scripts/install.sh https://gitlab.com/red-owl-games/unity-build/raw/master/scripts/install.sh
curl -sL -o ./scripts/build_macosx.sh https://gitlab.com/red-owl-games/unity-build/raw/master/scripts/build_macosx.sh
curl -sL -o ./scripts/build_webgl.sh https://gitlab.com/red-owl-games/unity-build/raw/master/scripts/build_webgl.sh
chmod a+x ./scripts/*.sh
