#!/bin/bash

project=$1
error_code=0

curl -sL -o $(pwd)Assets/RedOwlBuilder.cs https://gitlab.com/red-owl-games/unity-build/raw/master/scripts/RedOwlBuilder.cs

echo "[RedOwl] Building $project for WebGL."
$UNITY \
  -batchmode \
  -nographics \
  -silent-crashes \
  -logFile \
  -projectPath "$(pwd)" \
  -executeMethod RedOwlBuilder.build \
  -cacheServerEndpoint 192.168.1.218:10080 \
  -quit
if [ $? = 0 ] ; then
  echo "[RedOwl] Building WebGL completed successfully."
  error_code=0
else
  echo "[RedOwl] Building WebGL failed. Exited with $?."
  error_code=1
fi
exit $error_code
